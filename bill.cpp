#include <iostream>
#include <iomanip>

using namespace std;

const int REGULAR_BASE_PRICE = 10;
const int PREMIUM_BASE_PRICE = 25;

const int REGULAR_MINUTES_ALLOWED = 50;
const int PREMIUM_MINUTES_ALLOWED_AM = 75;
const int PREMIUM_MINUTES_ALLOWED_PM = 100;

const double REGULAR_RATE = .20;
const double PREMIUM_RATE_AM = .10;
const double PREMIUM_RATE_PM = .05;

void getAccountNumber(int &);
void getServiceCode(char &);
double getBill(char);
double getRegularBill();
double getPremiumBill();
void displayBill(int, char, double);

int main()
{
	int acct_num;
	char service_code;
	double bill_amount;

	getAccountNumber(acct_num);
	getServiceCode(service_code);

	bill_amount = getBill(service_code);
	displayBill(acct_num, service_code, bill_amount);
	return 0;
}

void getAccountNumber(int& acct_num)
{
	cout << "Please enter your account number: ";
	cin >> acct_num;
}

void getServiceCode(char& service_code)
{
	cout << "Service types - Regular (R) or Premium (P)" << endl;
	cout << "Please enter your service type: ";
	cin >> service_code;

	while (service_code != 'R' && service_code != 'r' && service_code != 'P' && service_code != 'p')
	{
		cout << "Invalid service type, please enter R or P." << endl;
		cout << "Please enter your service type: ";
		cin >> service_code;
	}
}

double getBill(char service_code)
{
	return (service_code == 'R' || service_code == 'r') ? getRegularBill() : getPremiumBill();
}

double getRegularBill()
{
	int used_minutes; // This name is a LOT better than "reg_minutes" imo.
	double bill_amount = REGULAR_BASE_PRICE;

	cout << "Please enter the total number of minutes used: ";
	cin >> used_minutes;

	if (used_minutes >= REGULAR_MINUTES_ALLOWED)
	{
		bill_amount += static_cast<double>(used_minutes - REGULAR_MINUTES_ALLOWED) * REGULAR_RATE;
	}

	return bill_amount;
}

double getPremiumBill()
{
	int used_minutes_am, used_minutes_pm;
	double bill_amount = PREMIUM_BASE_PRICE;

	cout << "Total number of minutes used between 6:00am and 6:00pm: ";
	cin >> used_minutes_am;
	cout << endl << "Total number of minutes used between 6:00am and 6:00pm: ";
	cin >> used_minutes_pm;

	if (used_minutes_am >= PREMIUM_MINUTES_ALLOWED_AM)
	{
		bill_amount += static_cast<double>(used_minutes_am - PREMIUM_MINUTES_ALLOWED_AM) * PREMIUM_RATE_AM;
	}

	if (used_minutes_pm >= PREMIUM_MINUTES_ALLOWED_PM)
	{
		bill_amount += static_cast<double>(used_minutes_pm - PREMIUM_MINUTES_ALLOWED_PM) * PREMIUM_RATE_PM;
	}

	return bill_amount;
}

void displayBill(int acct_num, char service_code, double bill_amount)
{
	cout << setprecision(2) << fixed;
	cout << endl << endl << endl;
	cout << "******************************" << endl;
	cout << "Account Number:\t" << acct_num << endl;
	cout << "Service Type:\t" << ((service_code == 'R' || service_code == 'r') ? "Regular" : "Premium") << endl;

	cout << "Total Charges:\t$" << bill_amount << endl;
	cout << "******************************" << endl;
}
